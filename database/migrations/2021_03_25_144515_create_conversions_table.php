<?php

use App\Models\Conversion;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateConversionsTable
 */
class CreateConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversions', function (Blueprint $table) {
            $table->bigIncrements(Conversion::ATTR_ID);
            $table->string(Conversion::ATTR_SOURCE_CURRENCY);
            $table->string(Conversion::ATTR_TARGET_CURRENCY);
            $table->decimal(Conversion::ATTR_AMOUNT, 20, 8);
            $table->decimal(Conversion::ATTR_AMOUNT_USD, 20, 8);
            $table->decimal(Conversion::ATTR_EXCHANGE_RATE, 20, 8);
            $table->decimal(Conversion::ATTR_CONVERSION_RESULT, 20, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversions');
    }
}
