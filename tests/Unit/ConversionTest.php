<?php

namespace Tests\Unit;

use App\Http\Requests\ConversionRequest;
use App\Services\ConversionCalculateService;

use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class ConversionTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function test_example(): void
    {

        $currenciesService = $this->getMockBuilder('App\Services\CurrenciesService')->enableProxyingToOriginalMethods()->getMock();
        $conversionService = new ConversionCalculateService($currenciesService);
        $result = $conversionService->getFormattedConversionData($this->getData());
        $this->assertIsArray($result);
    }

    /**
     * @return array
     */
    private function getData(): array
    {
        return [
            ConversionRequest::ATTR_AMOUNT => 1,
            ConversionRequest::ATTR_SOURCE_CURRENCY => 'USD',
            ConversionRequest::ATTR_TARGET_CURRENCY => 'CZK',
        ];
    }
}
