<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConversionRequest;
use App\Models\Conversion;
use App\Services\ConversionCalculateService;
use App\Services\ConversionStatisticsService;
use App\Services\CurrenciesService;
use Illuminate\Http\Request;

/**
 * Class ConversionController
 *
 * @package App\Http\Controllers
 */
class ConversionController extends Controller
{
    /**
     * @var \App\Services\CurrenciesService
     */
    private CurrenciesService $currenciesService;
    /**
     * @var \App\Services\ConversionCalculateService
     */
    private ConversionCalculateService $conversionCalculateService;
    /**
     * @var \App\Services\ConversionStatisticsService
     */
    private ConversionStatisticsService $conversionStatisticsService;

    /**
     * ConversionController constructor.
     *
     * @param \App\Services\CurrenciesService $currenciesService
     * @param \App\Services\ConversionCalculateService $conversionCalculateService
     * @param \App\Services\ConversionStatisticsService $conversionStatisticsService
     */
    public function __construct(
        CurrenciesService $currenciesService,
        ConversionCalculateService $conversionCalculateService,
        ConversionStatisticsService $conversionStatisticsService
    )
    {
        $this->currenciesService = $currenciesService;
        $this->conversionCalculateService = $conversionCalculateService;
        $this->conversionStatisticsService = $conversionStatisticsService;
    }

    /**
     * @param \App\Models\Conversion|null $conversion
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function index(Conversion $conversion = null)
    {
        $currencies = $this->currenciesService->getCurrencies();
        $statistics = $this->conversionStatisticsService->getStatistics();
        return view('layout', compact('currencies', 'statistics', 'conversion'));
    }

    /**
     * @param \App\Http\Requests\ConversionRequest $conversionRequest
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function convertAmount(ConversionRequest $conversionRequest): \Illuminate\Http\RedirectResponse
    {
        $data = $conversionRequest->validated();
        $conversionResult = $this->conversionCalculateService->getFormattedConversionData($data);
        $conversion = new Conversion();
        $conversion->compactFill($conversionResult);
        $conversion->save();
        return redirect()->route('home', $conversion);
    }
}
