<?php

namespace App\Http\Requests;

use App\Services\CurrenciesService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ConversionRequest
 *
 * @package App\Http\Requests
 */
class ConversionRequest extends FormRequest
{
    public const ATTR_AMOUNT ='amount';
    public const ATTR_SOURCE_CURRENCY ='source_currency';
    public const ATTR_TARGET_CURRENCY ='target_currency';

    /**
     * @var \App\Services\CurrenciesService
     */
    private CurrenciesService $currenciesService;

    /**
     * ConversionRequest constructor.
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null $content
     * @param \App\Services\CurrenciesService $currenciesService
     */
    public function __construct(
        CurrenciesService $currenciesService,
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->currenciesService=$currenciesService;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return mixed[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function rules(): array
    {
        $currencies =array_keys($this->currenciesService->getCurrencies());
        return [
            self::ATTR_AMOUNT=>'required|numeric|max:999999',
            self::ATTR_SOURCE_CURRENCY=>['required','string',Rule::in($currencies),'different:target_currency'],
            self::ATTR_TARGET_CURRENCY=>['required','string',Rule::in($currencies),'different:source_currency'],

        ];
    }
}
