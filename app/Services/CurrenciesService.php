<?php
declare(strict_types=1);

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

/**
 * Class CurrenciesService
 *
 * @package App\Services
 */
class CurrenciesService
{
    private const CURRENCIES_CACHE_KEY = 'currencies';
    private const CURRENCIES_CACHE_TIME_IN_SECONDS = 60 * 60;
    private const ATTR_RATES = 'rates';

    /**
     * @return mixed[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function getCurrencies(): array
    {
        Cache::pull(self::CURRENCIES_CACHE_KEY);
        $currencies = Cache::get(self::CURRENCIES_CACHE_KEY, null);
        if (!$currencies) {
            $currencies = $this->getLatestCurrenciesFromApi();
            Cache::put(self::CURRENCIES_CACHE_KEY, $currencies, self::CURRENCIES_CACHE_TIME_IN_SECONDS);
        }
        return $currencies;
    }

    /**
     * @return mixed[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    private function getLatestCurrenciesFromApi(): array
    {

        $client = new Client();
        $response = $client->get('https://openexchangerates.org/api/latest.json?app_id=ae064dec6a5f4b0dbdb4051b698dbd2b');
        if ($response->getStatusCode() === 200) {
            $result = \json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
            return $result[self::ATTR_RATES];
        }
        return [];
    }
}