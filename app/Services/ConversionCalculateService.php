<?php
declare(strict_types=1);

namespace App\Services;

use App\Http\Requests\ConversionRequest;
use App\Models\Conversion;

/**
 * Class ConversionCalculateService
 *
 * @package App\Services
 */
class ConversionCalculateService
{
    private const ATTR_SOURCE_CURRENCY = 'source_currency';
    private const ATTR_TARGET_CURRENCY = 'target_currency';
    private const ATTR_AMOUNT = 'amount';
    private ?array $currenciesArray = null;
    /**
     * @var \App\Services\CurrenciesService
     */
    private CurrenciesService $currenciesService;

    /**
     * ConversionCalculateService constructor.
     *
     * @param \App\Services\CurrenciesService $currenciesService
     */
    public function __construct(CurrenciesService $currenciesService)
    {
        $this->currenciesService = $currenciesService;
    }

    /**
     * @param mixed[] $data
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function convertData(array $data): string
    {
        return $this->calculateConversion($data);
    }

    /**
     * @param mixed[] $data
     * @return mixed[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function getFormattedConversionData(array $data): array
    {
        return [
            Conversion::ATTR_CONVERSION_RESULT => (float)$this->calculateConversion($data),
            Conversion::ATTR_EXCHANGE_RATE => (float)(string)$this->currenciesArray[$data[self::ATTR_TARGET_CURRENCY]],
            Conversion::ATTR_AMOUNT => (float)$data[ConversionRequest::ATTR_AMOUNT],
            Conversion::ATTR_SOURCE_CURRENCY => $data[ConversionRequest::ATTR_SOURCE_CURRENCY],
            Conversion::ATTR_TARGET_CURRENCY => $data[ConversionRequest::ATTR_TARGET_CURRENCY],
            Conversion::ATTR_AMOUNT_USD => (float)$this->calculateConversionToUSD($data),
        ];

    }

    /**
     * @param mixed[] $data
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    private function calculateConversion(array $data): string
    {
        $this->currenciesArray = $this->currenciesService->getCurrencies();
        return \bcmul($this->calculateConversionToUSD($data),
            (string)$this->currenciesArray[$data[ConversionRequest::ATTR_TARGET_CURRENCY]], 3);

    }

    /**
     * @param mixed[] $data
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    private function calculateConversionToUSD(array $data): string
    {
        $this->currenciesArray = $this->currenciesService->getCurrencies();
        return (string)($data[ConversionRequest::ATTR_AMOUNT] / (string)$this->currenciesArray[$data[ConversionRequest::ATTR_SOURCE_CURRENCY]]);
    }
}