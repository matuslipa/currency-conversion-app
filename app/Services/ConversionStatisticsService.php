<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Conversion;
use Carbon\CarbonImmutable;
use Illuminate\Support\Facades\DB;

/**
 * Class ConversionStatisticsService
 *
 * @package App\Services
 */
final class ConversionStatisticsService
{
    /**
     * @return mixed[]
     */
    public function getStatistics(): array
    {
        return [
            'conversion_total' => Conversion::query()->count(),
            'conversion_today' => Conversion::query()->whereDate(Conversion::ATTR_CREATED_AT,
                CarbonImmutable::today())->count(),
            'usd_total' => Conversion::query()->sum(Conversion::ATTR_AMOUNT_USD),
            'popular_currency' => Conversion::query()->select(Conversion::ATTR_TARGET_CURRENCY,
                DB::raw('count(*) as total'))
                                            ->groupBy(Conversion::ATTR_TARGET_CURRENCY)->orderBy('total',
                    'desc')->get()->first()->getTargetCurrency(),
        ];
    }
}