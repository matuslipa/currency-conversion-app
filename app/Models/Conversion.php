<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Conversion
 *
 * @package App\Models
 */
class Conversion extends Model
{
    use HasFactory;

    /**
     * Attributes of the model.
     */
    public const ATTR_ID = 'id';
    public const ATTR_AMOUNT = 'amount';
    public const ATTR_SOURCE_CURRENCY = 'source_currency';
    public const ATTR_TARGET_CURRENCY = 'target_currency';
    public const ATTR_AMOUNT_USD = 'amount_usd';
    public const ATTR_EXCHANGE_RATE = 'exchange_rate';
    public const ATTR_CONVERSION_RESULT = 'conversion_result';
    public const ATTR_CREATED_AT = self::CREATED_AT;

    /**
     * The attributes that are mass assignable.
     *
     * @var mixed[]
     */
    protected $fillable = [
        self::ATTR_AMOUNT,
        self::ATTR_SOURCE_CURRENCY,
        self::ATTR_TARGET_CURRENCY,
        self::ATTR_AMOUNT_USD,
        self::ATTR_EXCHANGE_RATE,
        self::ATTR_CONVERSION_RESULT,
    ];

    /**
     * @param mixed[] $data
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function compactFill(array $data): void
    {
        $this->fill($data);
    }

    /**
     * @return string
     */
    public function getSourceCurrency(): string
    {
        return $this->getAttributeValue(self::ATTR_SOURCE_CURRENCY);
    }

    /**
     * @return float
     */
    public function getExchangeRate(): float
    {
        return $this->getAttributeValue(self::ATTR_EXCHANGE_RATE);
    }

    /**
     * @return float
     */
    public function getConversionResult(): float
    {
        return $this->getAttributeValue(self::ATTR_CONVERSION_RESULT);
    }


    /**
     * @return string
     */
    public function getTargetCurrency(): string
    {
        return $this->getAttributeValue(self::ATTR_TARGET_CURRENCY);
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->getAttributeValue(self::ATTR_AMOUNT);
    }

    /**
     * @return float
     */
    public function getAmountInUSD(): float
    {
        return $this->getAttributeValue(self::ATTR_AMOUNT_USD);
    }
}
